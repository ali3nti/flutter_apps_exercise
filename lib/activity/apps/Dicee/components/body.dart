import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  var diceNumberLeft = 1;
  var diceNumberRight = 1;

  void randomNumber() {
    setState(() {
      diceNumberLeft = Random().nextInt(5) + 1;
      diceNumberRight = Random().nextInt(5) + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.red.shade400,
        body: SafeArea(
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: TextButton(
                      onPressed: () {
                        randomNumber();
                      },
                      child: Image.asset(
                          'assets/images/dicee/dice$diceNumberLeft.png')),
                ),
                SizedBox(
                  width: 5.0,
                ),
                Expanded(
                  flex: 1,
                  child: TextButton(
                    onPressed: () {
                      randomNumber();
                    },
                    child: Image(
                      image: AssetImage(
                          'assets/images/dicee/dice$diceNumberRight.png'),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
